﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Data;
using Tocronx.SimpleAsync;

namespace IfcFileInspector
{
    public class MainWindowViewModel : PropertyChangedModelBase
    {
        public CollectionViewSource IfcDataLinesSource { get; } = new();
        public ObservableCollection<IfcLine> SearchResult { get; } = new();
        public Command OpenIfcCommand { get; }
        public Command SearchCommand { get; }
        public Command<string> SelectIfcLineCommand { get; }
        public string FilterText { get => DictGet(""); set { if (DictSet(value)) { IfcDataLinesSource.View.Refresh(); } } }
        public string SearchText { get => DictGet(""); set => DictSet(value); }

        public event Action<IfcLine>? OnSelectIfcLine;

        private readonly Dictionary<string, IfcLine> lineByIdDict = new();
        private readonly ObservableCollection<IfcLine> ifcDataLines = new();

        public MainWindowViewModel()
        {
            IfcDataLinesSource.Source = ifcDataLines;
            IfcDataLinesSource.Filter += IfcDataLinesSource_Filter;
            OpenIfcCommand = new Command(OnOpenIfcCommand);
            SearchCommand = new Command(OnSearchCommand);
            SelectIfcLineCommand = new Command<string>(OnSelectIfcLineCommand);
        }

        private void IfcDataLinesSource_Filter(object sender, FilterEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(FilterText))
            {
                e.Accepted = true;
            }
            else if (e.Item is IfcLine ifcLine && ifcLine.Text.Contains(FilterText, StringComparison.OrdinalIgnoreCase))
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }

        private void IfcDataLinesSearchSource_Filter(object sender, FilterEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(FilterText))
            {
                e.Accepted = false;
            }
            else if (e.Item is IfcLine ifcLine && ifcLine.Text.Contains(FilterText, StringComparison.OrdinalIgnoreCase))
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }

        public void OnOpenIfcCommand()
        {
            OpenFileDialog openFileDialog = new()
            {
                Filter = "IFC (*.ifc)|*.ifc"
            };
            if (openFileDialog.ShowDialog() == true && openFileDialog.FileName != null)
            {
                IfcDataLinesSource.Filter -= IfcDataLinesSource_Filter;
                SearchResult.Clear();
                ifcDataLines.Clear();
                lineByIdDict.Clear();
                FilterText = "";
                var ifcData = IfcParser.ReadIfcFile(openFileDialog.FileName);
                if (ifcData is null) { return; }
                var numberLen = ifcData.Count.ToString().Length;
                int i = 0;
                foreach (var line in ifcData)
                {
                    var ifcLine = new IfcLine((++i).ToString().PadLeft(numberLen, ' '), line, SelectIfcLineCommand);
                    ifcDataLines.Add(ifcLine);
                    if (IfcParser.GetIfcIdFromLineStart(line) is { } id)
                    {
                        lineByIdDict.Add(id, ifcLine);
                    }
                }
                IfcDataLinesSource.Filter += IfcDataLinesSource_Filter;
            }
        }

        public void OnSearchCommand()
        {
            SearchResult.Clear();
            foreach (var ifcLine in ifcDataLines)
            {
                if (ifcLine.Text.Contains(SearchText, StringComparison.OrdinalIgnoreCase))
                {
                    SearchResult.Add(ifcLine);
                }
            }
        }

        public void OnSelectIfcLineCommand(string? ifcId)
        {
            if (ifcId is not null)
            {
                OnSelectIfcLine?.Invoke(lineByIdDict[ifcId]);
            }
        }

    }
}

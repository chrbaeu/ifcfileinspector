﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Documents;
using System.Windows.Media;
using Tocronx.SimpleAsync;

namespace IfcFileInspector
{
    public class IfcLine : PropertyChangedModelBase
    {
        public static readonly Regex ifcRefRegex = new("(#[0-9]*)");

        public string LineNumber { get; }
        public string Text { get; }
        public List<Inline> Inlines => GetInlines();

        private readonly Command<string> linkCommand;

        public IfcLine(string lineNumber, string text, Command<string> linkCommand)
        {
            LineNumber = lineNumber;
            Text = text;
            this.linkCommand = linkCommand;
        }

        public List<Inline> GetInlines()
        {
            var inlines = new List<Inline>() { new Run(LineNumber + "  ") { Foreground = Brushes.DarkGray } };
            foreach (var part in ifcRefRegex.Split(Text))
            {
                if (part.StartsWith('#'))
                {
                    inlines.Add(new Hyperlink(new Run(part)) { Command = linkCommand, CommandParameter = part });
                }
                else
                {
                    inlines.Add(new Run(part));
                }
            }
            return inlines;
        }

    }
}

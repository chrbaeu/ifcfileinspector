﻿using System;
using System.Windows;

namespace IfcFileInspector
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            if (DataContext is MainWindowViewModel viewModel)
            {
                viewModel.OnSelectIfcLine += ViewModel_OnSelectIfcLine;
            }
            Closed += MainWindow_Closed;
        }

        private void MainWindow_Closed(object? sender, EventArgs e)
        {
            if (DataContext is MainWindowViewModel viewModel)
            {
                viewModel.OnSelectIfcLine -= ViewModel_OnSelectIfcLine;
            }
        }

        private void ViewModel_OnSelectIfcLine(IfcLine item)
        {
            listBox.ScrollIntoView(item);
        }

    }
}

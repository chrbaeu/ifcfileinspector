﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace IfcFileInspector
{
    public static class TextBlockExtensions
    {
        public static readonly DependencyProperty BindableInlinesProperty =
            DependencyProperty.RegisterAttached("BindableInlines", typeof(IEnumerable<Inline>), typeof(TextBlockExtensions), new PropertyMetadata(null, OnBindableInlinesChanged));

        public static void SetBindableInlines(DependencyObject obj, IEnumerable<Inline> value)
            => obj.SetValue(BindableInlinesProperty, value);
        public static IEnumerable<Inline> GetBindableInlines(DependencyObject obj)
            => (IEnumerable<Inline>)obj.GetValue(BindableInlinesProperty);

        private static void OnBindableInlinesChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            if (obj is TextBlock Target)
            {
                Target.Inlines.Clear();
                Target.Inlines.AddRange((IEnumerable<Inline>)args.NewValue);
            }
        }

    }
}

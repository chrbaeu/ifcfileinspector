﻿using System;
using System.Collections.Generic;
using System.IO;

namespace IfcFileInspector
{
    public class IfcParser
    {
        public static string? GetIfcIdFromLineStart(string line)
        {
            if (line.Length == 0) { return null; }
            var span = line.AsSpan();
            if (span[0] != '#') { return null; }
            int idLen = 0;
            for (int i = 1; i < span.Length; i++)
            {

                if (char.IsDigit(span[i])) { continue; }
                if (span[i] != '=') { return null; }
                idLen = i;
                break;
            }
            if (idLen == 0) { return null; }
            return line.Substring(0, idLen);
        }

        public static List<string>? ReadIfcFile(string fileName)
        {
            List<string> dataLines = new();
            using var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read, 512 * 1024, FileOptions.SequentialScan);
            using var streamReader = new StreamReader(fileStream);
            string? line;
            while ((line = streamReader.ReadLine()) != null)
            {
                dataLines.Add(line);
            }
            return dataLines;
        }

        public static List<string>? ReadIfcData(string fileName)
        {
            List<string> dataLines = new();
            using var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read, 512 * 1024, FileOptions.SequentialScan);
            using var streamReader = new StreamReader(fileStream);
            string? line;
            bool dataSectionFound = false;
            while ((line = streamReader.ReadLine()) != null)
            {
                if ("DATA;" == line)
                {
                    dataSectionFound = true;
                    break;
                }
            }
            if (!dataSectionFound) { return null; }
            while ((line = streamReader.ReadLine()) != null)
            {
                if ("ENDSEC;" == line) { return dataLines; }
                if (line.Length == 0) { return null; }
                dataLines.Add(line);
            }
            return null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace IfcFileInspector
{
    public class PropertyChangedModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        protected bool raisePropertyChangedEvents = true;

        protected virtual bool Set<T>(ref T value, T newValue, [CallerMemberName] string propertyName = "")
        {
            if (newValue is string stringValue) { newValue = (T)(object)stringValue.Normalize(); }
            if (Equals(value, newValue)) { return false; }
            value = newValue;
            OnPropertyChanged(propertyName);
            return true;
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (raisePropertyChangedEvents)
            {
                VerifyPropertyName(propertyName);
                OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
            }
        }

        protected void OnPropertyChanged(PropertyChangedEventArgs propertyChangedEventArgs)
        {
            OnRaisePropertyChanged(propertyChangedEventArgs);
            PropertyChanged?.Invoke(this, propertyChangedEventArgs);
        }

        protected virtual void OnRaisePropertyChanged(PropertyChangedEventArgs propertyChangedEventArgs) { }

        [Conditional("DEBUG")]
        private void VerifyPropertyName(string propertyName)
        {
            if (this.GetType().GetProperties().FirstOrDefault(x => x.Name == propertyName) is null)
            {
                throw new InvalidOperationException($"Public property with name {propertyName} not found on type {this.GetType()}!");
            }
        }

        #region Dictionary Get/Set

        private Dictionary<string, object?>? propertyValues;

        [return: NotNullIfNotNull("defaultValue")]
        protected T? DictGet<T>(T? defaultValue = default, [CallerMemberName] string propertyName = "")
        {
            if (propertyValues == null) { propertyValues = new Dictionary<string, object?>(); }
            if (propertyValues.TryGetValue(propertyName, out var value)) { return (T?)value; }
            propertyValues[propertyName] = defaultValue;
            return defaultValue;
        }

        protected bool DictSet<T>(T value, [CallerMemberName] string propertyName = "")
        {
            if (value is string stringValue) { value = (T)(object)stringValue.Normalize(); }
            if (propertyValues == null) { propertyValues = new Dictionary<string, object?>(); }
            if (!propertyValues.TryGetValue(propertyName, out var currentValue))
            {
                currentValue = GetType().GetProperty(propertyName, BindingFlags.Instance | BindingFlags.Public)?.GetValue(this);
            }
            if (Equals(value, (T?)currentValue)) { return false; }
            propertyValues[propertyName] = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        #endregion

    }
}
